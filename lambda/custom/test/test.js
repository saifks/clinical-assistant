var expect = require('chai').expect;
var index = require('../index');

const context = require('aws-lambda-mock-context');
const ctx = context();

describe("Testing Launch request", function() {
    var speechResponse = null
    var speechError = null

    before(function(done){
        index.handler({
            "version": "1.0",
            "session": {
                "new": true,
                "sessionId": "amzn1.echo-api.session.4f1a8646-fcad-4d63-881c-a3628b30aa3d",
                "application": {
                    "applicationId": "amzn1.ask.skill.6ff2c877-21e3-4106-996b-112b78d0b895"
                },
                "user": {
                    "userId": "amzn1.ask.account.AF5CCYM5ZSJD77M6V5XMUDSEDC5B67NUKEAUIJKB4KKHULPMXYPH7Y2WWWWFZBI327JWUJ4TVGF6MLQRAZKISFX27G4OIW7P2MNQ3RPZZXDI6TJKDCBMEMEP2IFMLPZ5ZQNVIIITBWQ73MED6NVGDXPZO3VC2CENWNGN2EHNQB4RBRH7H4VJZ43ABKHGSSEO4PZEEAUPMNZFZMI"
                }
            },
            "context": {
                "AudioPlayer": {
                    "playerActivity": "IDLE"
                },
                "Display": {},
                "System": {
                    "application": {
                        "applicationId": "amzn1.ask.skill.6ff2c877-21e3-4106-996b-112b78d0b895"
                    },
                    "user": {
                        "userId": "amzn1.ask.account.AF5CCYM5ZSJD77M6V5XMUDSEDC5B67NUKEAUIJKB4KKHULPMXYPH7Y2WWWWFZBI327JWUJ4TVGF6MLQRAZKISFX27G4OIW7P2MNQ3RPZZXDI6TJKDCBMEMEP2IFMLPZ5ZQNVIIITBWQ73MED6NVGDXPZO3VC2CENWNGN2EHNQB4RBRH7H4VJZ43ABKHGSSEO4PZEEAUPMNZFZMI"
                    },
                    "device": {
                        "deviceId": "amzn1.ask.device.AELI2BPV7TCH6VT4RSYYGTX73XS4XPTZGWKNOOPOO6G27V3WYISPLMVTMWFS7CGDAKPLHGPGDBLG6VHAEKMPMYA2IXWVY5GEL677AP25E2EBWPO4KS4YONWATDD7YAIQOYQFGK3UPEOIDMWOA6GMAJERUYEI6LLBCYZTW5UWY336B4Y7YTPUM",
                        "supportedInterfaces": {
                            "AudioPlayer": {},
                            "Display": {
                                "templateVersion": "1.0",
                                "markupVersion": "1.0"
                            }
                        }
                    },
                    "apiEndpoint": "https://api.eu.amazonalexa.com",
                    "apiAccessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLjZmZjJjODc3LTIxZTMtNDEwNi05OTZiLTExMmI3OGQwYjg5NSIsImV4cCI6MTUyNDM4MTA4MywiaWF0IjoxNTI0Mzc3NDgzLCJuYmYiOjE1MjQzNzc0ODMsInByaXZhdGVDbGFpbXMiOnsiY29uc2VudFRva2VuIjpudWxsLCJkZXZpY2VJZCI6ImFtem4xLmFzay5kZXZpY2UuQUVMSTJCUFY3VENINlZUNFJTWVlHVFg3M1hTNFhQVFpHV0tOT09QT082RzI3VjNXWUlTUExNVlRNV0ZTN0NHREFLUExIR1BHREJMRzZWSEFFS01QTVlBMklYV1ZZNUdFTDY3N0FQMjVFMkVCV1BPNEtTNFlPTldBVEREN1lBSVFPWVFGR0szVVBFT0lETVdPQTZHTUFKRVJVWUVJNkxMQkNZWlRXNVVXWTMzNkI0WTdZVFBVTSIsInVzZXJJZCI6ImFtem4xLmFzay5hY2NvdW50LkFGNUNDWU01WlNKRDc3TTZWNVhNVURTRURDNUI2N05VS0VBVUlKS0I0S0tIVUxQTVhZUEg3WTJXV1dXRlpCSTMyN0pXVUo0VFZHRjZNTFFSQVpLSVNGWDI3RzRPSVc3UDJNTlEzUlBaWlhESTZUSktEQ0JNRU1FUDJJRk1MUFo1WlFOVklJSVRCV1E3M01FRDZOVkdEWFBaTzNWQzJDRU5XTkdOMkVITlFCNFJCUkg3SDRWSlo0M0FCS0hHU1NFTzRQWkVFQVVQTU5aRlpNSSJ9fQ.PXje-dOzzYgCqBnBUzGe_Qdzf7TL24FJ6tn8JbvgyotCJCigLwYBcfDzJCKloJq5FcfN2Id0BunkvW5eL7z-G1P3EUn9XH0xszRaClw-IDRV-TDy505l9_21IJvQwZwjFDq_a_QdX4QD5NsxHXbzGB1RUHk7lgMq4GTW900Dad6dJuF1NsP-sVsqSUEg9CsgvXE97RXd_JrRQvqL1SwW2U1KG6q2eS-IhhPdTmeI36xUfW0aHwfPKH227sxnO1O5rodE4AC7EKhWEZTCJR3_9nKf9c3A5S9rhfbEGAIIfwyvWsUz3xT2M_ukWF5cH_3MuJKEU-aPPJbxKvZKnfW4YA"
                }
            },
            "request": {
                "type": "LaunchRequest",
                "requestId": "amzn1.echo-api.request.47dfca9c-5411-4a33-9960-eb10f25fc086",
                "timestamp": "2018-04-22T06:11:23Z",
                "locale": "en-IN",
                "shouldLinkResultBeReturned": false
            }
        }, ctx)

        ctx.Promise
            .then(resp => { speechResponse = resp; done(); })
            .catch(err => { speechError = err; done(); })
    })

    describe("The response is structurally correct for launch request", function() {
        it('should not have errored',function() {
            expect(speechError).to.be.null
        })

        it('should have a version', function() {
            expect(speechResponse.version).not.to.be.null
        })

        it('should have a speechlet response', function() {
            expect(speechResponse.response).not.to.be.null
        })

        it("should have a spoken response", () => {
            expect(speechResponse.response.outputSpeech).not.to.be.null
        })
        it("should have a spoken response ssml", () => {
            expect(speechResponse.response.outputSpeech.ssml).not.to.be.null
        })
        it("should end the alexa session", function() {
            expect(speechResponse.response.shouldEndSession).not.to.be.null
            expect(speechResponse.response.shouldEndSession).to.be.false
        })
    })
})

describe("Testing RecordPrescriptionIntent", function() {
    var speechResponse = null
    var speechError = null

    before(function(done){
        index.handler({
            "version": "1.0",
            "session": {
                "new": false,
                "sessionId": "amzn1.echo-api.session.4f1a8646-fcad-4d63-881c-a3628b30aa3d",
                "application": {
                    "applicationId": "amzn1.ask.skill.6ff2c877-21e3-4106-996b-112b78d0b895"
                },
                "attributes": {
                    "STATE": "_STARTMODE"
                },
                "user": {
                    "userId": "amzn1.ask.account.AF5CCYM5ZSJD77M6V5XMUDSEDC5B67NUKEAUIJKB4KKHULPMXYPH7Y2WWWWFZBI327JWUJ4TVGF6MLQRAZKISFX27G4OIW7P2MNQ3RPZZXDI6TJKDCBMEMEP2IFMLPZ5ZQNVIIITBWQ73MED6NVGDXPZO3VC2CENWNGN2EHNQB4RBRH7H4VJZ43ABKHGSSEO4PZEEAUPMNZFZMI"
                }
            },
            "context": {
                "AudioPlayer": {
                    "playerActivity": "IDLE"
                },
                "Display": {
                    "token": ""
                },
                "System": {
                    "application": {
                        "applicationId": "amzn1.ask.skill.6ff2c877-21e3-4106-996b-112b78d0b895"
                    },
                    "user": {
                        "userId": "amzn1.ask.account.AF5CCYM5ZSJD77M6V5XMUDSEDC5B67NUKEAUIJKB4KKHULPMXYPH7Y2WWWWFZBI327JWUJ4TVGF6MLQRAZKISFX27G4OIW7P2MNQ3RPZZXDI6TJKDCBMEMEP2IFMLPZ5ZQNVIIITBWQ73MED6NVGDXPZO3VC2CENWNGN2EHNQB4RBRH7H4VJZ43ABKHGSSEO4PZEEAUPMNZFZMI"
                    },
                    "device": {
                        "deviceId": "amzn1.ask.device.AELI2BPV7TCH6VT4RSYYGTX73XS4XPTZGWKNOOPOO6G27V3WYISPLMVTMWFS7CGDAKPLHGPGDBLG6VHAEKMPMYA2IXWVY5GEL677AP25E2EBWPO4KS4YONWATDD7YAIQOYQFGK3UPEOIDMWOA6GMAJERUYEI6LLBCYZTW5UWY336B4Y7YTPUM",
                        "supportedInterfaces": {
                            "AudioPlayer": {},
                            "Display": {
                                "templateVersion": "1.0",
                                "markupVersion": "1.0"
                            }
                        }
                    },
                    "apiEndpoint": "https://api.eu.amazonalexa.com",
                    "apiAccessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLjZmZjJjODc3LTIxZTMtNDEwNi05OTZiLTExMmI3OGQwYjg5NSIsImV4cCI6MTUyNDM4MTE0OSwiaWF0IjoxNTI0Mzc3NTQ5LCJuYmYiOjE1MjQzNzc1NDksInByaXZhdGVDbGFpbXMiOnsiY29uc2VudFRva2VuIjpudWxsLCJkZXZpY2VJZCI6ImFtem4xLmFzay5kZXZpY2UuQUVMSTJCUFY3VENINlZUNFJTWVlHVFg3M1hTNFhQVFpHV0tOT09QT082RzI3VjNXWUlTUExNVlRNV0ZTN0NHREFLUExIR1BHREJMRzZWSEFFS01QTVlBMklYV1ZZNUdFTDY3N0FQMjVFMkVCV1BPNEtTNFlPTldBVEREN1lBSVFPWVFGR0szVVBFT0lETVdPQTZHTUFKRVJVWUVJNkxMQkNZWlRXNVVXWTMzNkI0WTdZVFBVTSIsInVzZXJJZCI6ImFtem4xLmFzay5hY2NvdW50LkFGNUNDWU01WlNKRDc3TTZWNVhNVURTRURDNUI2N05VS0VBVUlKS0I0S0tIVUxQTVhZUEg3WTJXV1dXRlpCSTMyN0pXVUo0VFZHRjZNTFFSQVpLSVNGWDI3RzRPSVc3UDJNTlEzUlBaWlhESTZUSktEQ0JNRU1FUDJJRk1MUFo1WlFOVklJSVRCV1E3M01FRDZOVkdEWFBaTzNWQzJDRU5XTkdOMkVITlFCNFJCUkg3SDRWSlo0M0FCS0hHU1NFTzRQWkVFQVVQTU5aRlpNSSJ9fQ.BC1aHEnxwIwkSygnDoO5XV_1PGJQwfm0fOsT8zaZf5ATpxL3dXrwXx30o1AXc1BwCJPuHI-dX3yeibaZYSY3hVYg_Dv6jkFNsLUTHwa5FQzi2odAfdMqRoq-esNeqPuxYNLrOw3Gwi6XXL2x7bi6V9UjBz6SghEkPZYHrCRNBajILvnq8mvpk_8n7_kb7jCbu8v3IYifHB-lJoBfcviE1O0voJRu96Yw8jOEB9E03Wn41i9-UvZOUQubxjvddb46vORCoDl39ha8v3WlJFNk92bSZxj-wUr08px6hmptnCvf8YpNUs25uZ2EI5FW5St5Oxe2Gi8GLZ0PMa00Lt9uAw"
                }
            },
            "request": {
                "type": "IntentRequest",
                "requestId": "amzn1.echo-api.request.44fcb3bc-7ccc-4652-bd11-c8843547bc94",
                "timestamp": "2018-04-22T06:12:29Z",
                "locale": "en-IN",
                "intent": {
                    "name": "RecordPrescriptionIntent",
                    "confirmationStatus": "NONE",
                    "slots": {
                        "date": {
                            "name": "date",
                            "confirmationStatus": "NONE"
                        },
                        "followup": {
                            "name": "followup",
                            "confirmationStatus": "NONE"
                        },
                        "PatientFirstName": {
                            "name": "PatientFirstName",
                            "value": "Ali",
                            "confirmationStatus": "NONE"
                        },
                        "gender": {
                            "name": "gender",
                            "confirmationStatus": "NONE"
                        },
                        "PatientLastName": {
                            "name": "PatientLastName",
                            "confirmationStatus": "NONE"
                        },
                        "when": {
                            "name": "when",
                            "confirmationStatus": "NONE"
                        },
                        "age": {
                            "name": "age",
                            "confirmationStatus": "NONE"
                        },
                        "contactno": {
                            "name": "contactno",
                            "confirmationStatus": "NONE"
                        }
                    }
                },
                "dialogState": "STARTED"
            }
        }, ctx)

        ctx.Promise
            .then(resp => { speechResponse = resp; done(); })
            .catch(err => { speechError = err; done(); })
    })

    describe("The response is structurally correct for RecordPrescriptionIntent", function() {
        it('should not have errored',function() {
            expect(speechError).to.be.null
        })

        it('should have a version', function() {
            expect(speechResponse.version).not.to.be.null
        })

        it('should have a speechlet response', function() {
            expect(speechResponse.response).not.to.be.null
        })

        it("should have a spoken response", () => {
            expect(speechResponse.response.outputSpeech).not.to.be.null
        })
        it("should have a spoken response ssml", () => {
            expect(speechResponse.response.outputSpeech.ssml).not.to.be.null
        })
        it("should end the alexa session", function() {
            expect(speechResponse.response.shouldEndSession).not.to.be.null
            expect(speechResponse.response.shouldEndSession).to.be.false
        })
    })
})

describe("Testing BookAppointmentIntent", function() {
    var speechResponse = null
    var speechError = null

    before(function(done){
        index.handler({
            "version": "1.0",
            "session": {
                "new": false,
                "sessionId": "amzn1.echo-api.session.4f1a8646-fcad-4d63-881c-a3628b30aa3d",
                "application": {
                    "applicationId": "amzn1.ask.skill.6ff2c877-21e3-4106-996b-112b78d0b895"
                },
                "attributes": {
                    "STATE": "_STARTMODE"
                },
                "user": {
                    "userId": "amzn1.ask.account.AF5CCYM5ZSJD77M6V5XMUDSEDC5B67NUKEAUIJKB4KKHULPMXYPH7Y2WWWWFZBI327JWUJ4TVGF6MLQRAZKISFX27G4OIW7P2MNQ3RPZZXDI6TJKDCBMEMEP2IFMLPZ5ZQNVIIITBWQ73MED6NVGDXPZO3VC2CENWNGN2EHNQB4RBRH7H4VJZ43ABKHGSSEO4PZEEAUPMNZFZMI"
                }
            },
            "context": {
                "AudioPlayer": {
                    "playerActivity": "IDLE"
                },
                "Display": {
                    "token": ""
                },
                "System": {
                    "application": {
                        "applicationId": "amzn1.ask.skill.6ff2c877-21e3-4106-996b-112b78d0b895"
                    },
                    "user": {
                        "userId": "amzn1.ask.account.AF5CCYM5ZSJD77M6V5XMUDSEDC5B67NUKEAUIJKB4KKHULPMXYPH7Y2WWWWFZBI327JWUJ4TVGF6MLQRAZKISFX27G4OIW7P2MNQ3RPZZXDI6TJKDCBMEMEP2IFMLPZ5ZQNVIIITBWQ73MED6NVGDXPZO3VC2CENWNGN2EHNQB4RBRH7H4VJZ43ABKHGSSEO4PZEEAUPMNZFZMI"
                    },
                    "device": {
                        "deviceId": "amzn1.ask.device.AELI2BPV7TCH6VT4RSYYGTX73XS4XPTZGWKNOOPOO6G27V3WYISPLMVTMWFS7CGDAKPLHGPGDBLG6VHAEKMPMYA2IXWVY5GEL677AP25E2EBWPO4KS4YONWATDD7YAIQOYQFGK3UPEOIDMWOA6GMAJERUYEI6LLBCYZTW5UWY336B4Y7YTPUM",
                        "supportedInterfaces": {
                            "AudioPlayer": {},
                            "Display": {
                                "templateVersion": "1.0",
                                "markupVersion": "1.0"
                            }
                        }
                    },
                    "apiEndpoint": "https://api.eu.amazonalexa.com",
                    "apiAccessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLjZmZjJjODc3LTIxZTMtNDEwNi05OTZiLTExMmI3OGQwYjg5NSIsImV4cCI6MTUyNDM4MTE0OSwiaWF0IjoxNTI0Mzc3NTQ5LCJuYmYiOjE1MjQzNzc1NDksInByaXZhdGVDbGFpbXMiOnsiY29uc2VudFRva2VuIjpudWxsLCJkZXZpY2VJZCI6ImFtem4xLmFzay5kZXZpY2UuQUVMSTJCUFY3VENINlZUNFJTWVlHVFg3M1hTNFhQVFpHV0tOT09QT082RzI3VjNXWUlTUExNVlRNV0ZTN0NHREFLUExIR1BHREJMRzZWSEFFS01QTVlBMklYV1ZZNUdFTDY3N0FQMjVFMkVCV1BPNEtTNFlPTldBVEREN1lBSVFPWVFGR0szVVBFT0lETVdPQTZHTUFKRVJVWUVJNkxMQkNZWlRXNVVXWTMzNkI0WTdZVFBVTSIsInVzZXJJZCI6ImFtem4xLmFzay5hY2NvdW50LkFGNUNDWU01WlNKRDc3TTZWNVhNVURTRURDNUI2N05VS0VBVUlKS0I0S0tIVUxQTVhZUEg3WTJXV1dXRlpCSTMyN0pXVUo0VFZHRjZNTFFSQVpLSVNGWDI3RzRPSVc3UDJNTlEzUlBaWlhESTZUSktEQ0JNRU1FUDJJRk1MUFo1WlFOVklJSVRCV1E3M01FRDZOVkdEWFBaTzNWQzJDRU5XTkdOMkVITlFCNFJCUkg3SDRWSlo0M0FCS0hHU1NFTzRQWkVFQVVQTU5aRlpNSSJ9fQ.BC1aHEnxwIwkSygnDoO5XV_1PGJQwfm0fOsT8zaZf5ATpxL3dXrwXx30o1AXc1BwCJPuHI-dX3yeibaZYSY3hVYg_Dv6jkFNsLUTHwa5FQzi2odAfdMqRoq-esNeqPuxYNLrOw3Gwi6XXL2x7bi6V9UjBz6SghEkPZYHrCRNBajILvnq8mvpk_8n7_kb7jCbu8v3IYifHB-lJoBfcviE1O0voJRu96Yw8jOEB9E03Wn41i9-UvZOUQubxjvddb46vORCoDl39ha8v3WlJFNk92bSZxj-wUr08px6hmptnCvf8YpNUs25uZ2EI5FW5St5Oxe2Gi8GLZ0PMa00Lt9uAw"
                }
            },
            "request": {
                "type": "IntentRequest",
                "requestId": "amzn1.echo-api.request.44fcb3bc-7ccc-4652-bd11-c8843547bc94",
                "timestamp": "2018-04-22T06:12:29Z",
                "locale": "en-IN",
                "intent": {
                    "name": "BookAppointmentIntent",
                    "confirmationStatus": "NONE",
                    "slots": {
                        "date": {
                            "name": "date",
                            "confirmationStatus": "NONE"
                        },
                        "followup": {
                            "name": "followup",
                            "confirmationStatus": "NONE"
                        },
                        "PatientFirstName": {
                            "name": "PatientFirstName",
                            "value": "Ali",
                            "confirmationStatus": "NONE"
                        },
                        "gender": {
                            "name": "gender",
                            "confirmationStatus": "NONE"
                        },
                        "PatientLastName": {
                            "name": "PatientLastName",
                            "confirmationStatus": "NONE"
                        },
                        "when": {
                            "name": "when",
                            "confirmationStatus": "NONE"
                        },
                        "age": {
                            "name": "age",
                            "confirmationStatus": "NONE"
                        },
                        "contactno": {
                            "name": "contactno",
                            "confirmationStatus": "NONE"
                        }
                    }
                },
                "dialogState": "STARTED"
            }
        }, ctx)

        ctx.Promise
            .then(resp => { speechResponse = resp; done(); })
            .catch(err => { speechError = err; done(); })
    })

    describe("The response is structurally correct for BookAppointmentIntent", function() {
        it('should not have errored',function() {
            expect(speechError).to.be.null
        })

        it('should have a version', function() {
            expect(speechResponse.version).not.to.be.null
        })

        it('should have a speechlet response', function() {
            expect(speechResponse.response).not.to.be.null
        })

        it("should have a spoken response", () => {
            expect(speechResponse.response.outputSpeech).not.to.be.null
        })
        it("should have a spoken response ssml", () => {
            expect(speechResponse.response.outputSpeech.ssml).not.to.be.null
        })
        it("should end the alexa session", function() {
            expect(speechResponse.response.shouldEndSession).not.to.be.null
            expect(speechResponse.response.shouldEndSession).to.be.false
        })
    })
})

describe("Testing CheckinPatient", function() {
    var speechResponse = null
    var speechError = null

    before(function(done){
        index.handler({
            "version": "1.0",
            "session": {
                "new": false,
                "sessionId": "amzn1.echo-api.session.4f1a8646-fcad-4d63-881c-a3628b30aa3d",
                "application": {
                    "applicationId": "amzn1.ask.skill.6ff2c877-21e3-4106-996b-112b78d0b895"
                },
                "attributes": {
                    "STATE": "_STARTMODE"
                },
                "user": {
                    "userId": "amzn1.ask.account.AF5CCYM5ZSJD77M6V5XMUDSEDC5B67NUKEAUIJKB4KKHULPMXYPH7Y2WWWWFZBI327JWUJ4TVGF6MLQRAZKISFX27G4OIW7P2MNQ3RPZZXDI6TJKDCBMEMEP2IFMLPZ5ZQNVIIITBWQ73MED6NVGDXPZO3VC2CENWNGN2EHNQB4RBRH7H4VJZ43ABKHGSSEO4PZEEAUPMNZFZMI"
                }
            },
            "context": {
                "AudioPlayer": {
                    "playerActivity": "IDLE"
                },
                "Display": {
                    "token": ""
                },
                "System": {
                    "application": {
                        "applicationId": "amzn1.ask.skill.6ff2c877-21e3-4106-996b-112b78d0b895"
                    },
                    "user": {
                        "userId": "amzn1.ask.account.AF5CCYM5ZSJD77M6V5XMUDSEDC5B67NUKEAUIJKB4KKHULPMXYPH7Y2WWWWFZBI327JWUJ4TVGF6MLQRAZKISFX27G4OIW7P2MNQ3RPZZXDI6TJKDCBMEMEP2IFMLPZ5ZQNVIIITBWQ73MED6NVGDXPZO3VC2CENWNGN2EHNQB4RBRH7H4VJZ43ABKHGSSEO4PZEEAUPMNZFZMI"
                    },
                    "device": {
                        "deviceId": "amzn1.ask.device.AELI2BPV7TCH6VT4RSYYGTX73XS4XPTZGWKNOOPOO6G27V3WYISPLMVTMWFS7CGDAKPLHGPGDBLG6VHAEKMPMYA2IXWVY5GEL677AP25E2EBWPO4KS4YONWATDD7YAIQOYQFGK3UPEOIDMWOA6GMAJERUYEI6LLBCYZTW5UWY336B4Y7YTPUM",
                        "supportedInterfaces": {
                            "AudioPlayer": {},
                            "Display": {
                                "templateVersion": "1.0",
                                "markupVersion": "1.0"
                            }
                        }
                    },
                    "apiEndpoint": "https://api.eu.amazonalexa.com",
                    "apiAccessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLjZmZjJjODc3LTIxZTMtNDEwNi05OTZiLTExMmI3OGQwYjg5NSIsImV4cCI6MTUyNDM4MTE0OSwiaWF0IjoxNTI0Mzc3NTQ5LCJuYmYiOjE1MjQzNzc1NDksInByaXZhdGVDbGFpbXMiOnsiY29uc2VudFRva2VuIjpudWxsLCJkZXZpY2VJZCI6ImFtem4xLmFzay5kZXZpY2UuQUVMSTJCUFY3VENINlZUNFJTWVlHVFg3M1hTNFhQVFpHV0tOT09QT082RzI3VjNXWUlTUExNVlRNV0ZTN0NHREFLUExIR1BHREJMRzZWSEFFS01QTVlBMklYV1ZZNUdFTDY3N0FQMjVFMkVCV1BPNEtTNFlPTldBVEREN1lBSVFPWVFGR0szVVBFT0lETVdPQTZHTUFKRVJVWUVJNkxMQkNZWlRXNVVXWTMzNkI0WTdZVFBVTSIsInVzZXJJZCI6ImFtem4xLmFzay5hY2NvdW50LkFGNUNDWU01WlNKRDc3TTZWNVhNVURTRURDNUI2N05VS0VBVUlKS0I0S0tIVUxQTVhZUEg3WTJXV1dXRlpCSTMyN0pXVUo0VFZHRjZNTFFSQVpLSVNGWDI3RzRPSVc3UDJNTlEzUlBaWlhESTZUSktEQ0JNRU1FUDJJRk1MUFo1WlFOVklJSVRCV1E3M01FRDZOVkdEWFBaTzNWQzJDRU5XTkdOMkVITlFCNFJCUkg3SDRWSlo0M0FCS0hHU1NFTzRQWkVFQVVQTU5aRlpNSSJ9fQ.BC1aHEnxwIwkSygnDoO5XV_1PGJQwfm0fOsT8zaZf5ATpxL3dXrwXx30o1AXc1BwCJPuHI-dX3yeibaZYSY3hVYg_Dv6jkFNsLUTHwa5FQzi2odAfdMqRoq-esNeqPuxYNLrOw3Gwi6XXL2x7bi6V9UjBz6SghEkPZYHrCRNBajILvnq8mvpk_8n7_kb7jCbu8v3IYifHB-lJoBfcviE1O0voJRu96Yw8jOEB9E03Wn41i9-UvZOUQubxjvddb46vORCoDl39ha8v3WlJFNk92bSZxj-wUr08px6hmptnCvf8YpNUs25uZ2EI5FW5St5Oxe2Gi8GLZ0PMa00Lt9uAw"
                }
            },
            "request": {
                "type": "IntentRequest",
                "requestId": "amzn1.echo-api.request.44fcb3bc-7ccc-4652-bd11-c8843547bc94",
                "timestamp": "2018-04-22T06:12:29Z",
                "locale": "en-IN",
                "intent": {
                    "name": "CheckinPatient",
                    "confirmationStatus": "NONE",
                    "slots": {
                        "date": {
                            "name": "date",
                            "confirmationStatus": "NONE"
                        },
                        "followup": {
                            "name": "followup",
                            "confirmationStatus": "NONE"
                        },
                        "PatientFirstName": {
                            "name": "PatientFirstName",
                            "value": "Ali",
                            "confirmationStatus": "NONE"
                        },
                        "gender": {
                            "name": "gender",
                            "confirmationStatus": "NONE"
                        },
                        "PatientLastName": {
                            "name": "PatientLastName",
                            "confirmationStatus": "NONE"
                        },
                        "when": {
                            "name": "when",
                            "confirmationStatus": "NONE"
                        },
                        "age": {
                            "name": "age",
                            "confirmationStatus": "NONE"
                        },
                        "contactno": {
                            "name": "contactno",
                            "confirmationStatus": "NONE"
                        }
                    }
                },
                "dialogState": "STARTED"
            }
        }, ctx)

        ctx.Promise
            .then(resp => { speechResponse = resp; done(); })
            .catch(err => { speechError = err; done(); })
    })

    describe("The response is structurally correct for CheckinPatient", function() {
        it('should not have errored',function() {
            expect(speechError).to.be.null
        })

        it('should have a version', function() {
            expect(speechResponse.version).not.to.be.null
        })

        it('should have a speechlet response', function() {
            expect(speechResponse.response).not.to.be.null
        })

        it("should have a spoken response", () => {
            expect(speechResponse.response.outputSpeech).not.to.be.null
        })
        it("should have a spoken response ssml", () => {
            expect(speechResponse.response.outputSpeech.ssml).not.to.be.null
        })
        it("should end the alexa session", function() {
            expect(speechResponse.response.shouldEndSession).not.to.be.null
            expect(speechResponse.response.shouldEndSession).to.be.false
        })
    })
})
describe("Testing CheckOutPatient", function() {
    var speechResponse = null
    var speechError = null

    before(function(done){
        index.handler({
            "version": "1.0",
            "session": {
                "new": false,
                "sessionId": "amzn1.echo-api.session.4f1a8646-fcad-4d63-881c-a3628b30aa3d",
                "application": {
                    "applicationId": "amzn1.ask.skill.6ff2c877-21e3-4106-996b-112b78d0b895"
                },
                "attributes": {
                    "STATE": "_STARTMODE"
                },
                "user": {
                    "userId": "amzn1.ask.account.AF5CCYM5ZSJD77M6V5XMUDSEDC5B67NUKEAUIJKB4KKHULPMXYPH7Y2WWWWFZBI327JWUJ4TVGF6MLQRAZKISFX27G4OIW7P2MNQ3RPZZXDI6TJKDCBMEMEP2IFMLPZ5ZQNVIIITBWQ73MED6NVGDXPZO3VC2CENWNGN2EHNQB4RBRH7H4VJZ43ABKHGSSEO4PZEEAUPMNZFZMI"
                }
            },
            "context": {
                "AudioPlayer": {
                    "playerActivity": "IDLE"
                },
                "Display": {
                    "token": ""
                },
                "System": {
                    "application": {
                        "applicationId": "amzn1.ask.skill.6ff2c877-21e3-4106-996b-112b78d0b895"
                    },
                    "user": {
                        "userId": "amzn1.ask.account.AF5CCYM5ZSJD77M6V5XMUDSEDC5B67NUKEAUIJKB4KKHULPMXYPH7Y2WWWWFZBI327JWUJ4TVGF6MLQRAZKISFX27G4OIW7P2MNQ3RPZZXDI6TJKDCBMEMEP2IFMLPZ5ZQNVIIITBWQ73MED6NVGDXPZO3VC2CENWNGN2EHNQB4RBRH7H4VJZ43ABKHGSSEO4PZEEAUPMNZFZMI"
                    },
                    "device": {
                        "deviceId": "amzn1.ask.device.AELI2BPV7TCH6VT4RSYYGTX73XS4XPTZGWKNOOPOO6G27V3WYISPLMVTMWFS7CGDAKPLHGPGDBLG6VHAEKMPMYA2IXWVY5GEL677AP25E2EBWPO4KS4YONWATDD7YAIQOYQFGK3UPEOIDMWOA6GMAJERUYEI6LLBCYZTW5UWY336B4Y7YTPUM",
                        "supportedInterfaces": {
                            "AudioPlayer": {},
                            "Display": {
                                "templateVersion": "1.0",
                                "markupVersion": "1.0"
                            }
                        }
                    },
                    "apiEndpoint": "https://api.eu.amazonalexa.com",
                    "apiAccessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLjZmZjJjODc3LTIxZTMtNDEwNi05OTZiLTExMmI3OGQwYjg5NSIsImV4cCI6MTUyNDM4MTE0OSwiaWF0IjoxNTI0Mzc3NTQ5LCJuYmYiOjE1MjQzNzc1NDksInByaXZhdGVDbGFpbXMiOnsiY29uc2VudFRva2VuIjpudWxsLCJkZXZpY2VJZCI6ImFtem4xLmFzay5kZXZpY2UuQUVMSTJCUFY3VENINlZUNFJTWVlHVFg3M1hTNFhQVFpHV0tOT09QT082RzI3VjNXWUlTUExNVlRNV0ZTN0NHREFLUExIR1BHREJMRzZWSEFFS01QTVlBMklYV1ZZNUdFTDY3N0FQMjVFMkVCV1BPNEtTNFlPTldBVEREN1lBSVFPWVFGR0szVVBFT0lETVdPQTZHTUFKRVJVWUVJNkxMQkNZWlRXNVVXWTMzNkI0WTdZVFBVTSIsInVzZXJJZCI6ImFtem4xLmFzay5hY2NvdW50LkFGNUNDWU01WlNKRDc3TTZWNVhNVURTRURDNUI2N05VS0VBVUlKS0I0S0tIVUxQTVhZUEg3WTJXV1dXRlpCSTMyN0pXVUo0VFZHRjZNTFFSQVpLSVNGWDI3RzRPSVc3UDJNTlEzUlBaWlhESTZUSktEQ0JNRU1FUDJJRk1MUFo1WlFOVklJSVRCV1E3M01FRDZOVkdEWFBaTzNWQzJDRU5XTkdOMkVITlFCNFJCUkg3SDRWSlo0M0FCS0hHU1NFTzRQWkVFQVVQTU5aRlpNSSJ9fQ.BC1aHEnxwIwkSygnDoO5XV_1PGJQwfm0fOsT8zaZf5ATpxL3dXrwXx30o1AXc1BwCJPuHI-dX3yeibaZYSY3hVYg_Dv6jkFNsLUTHwa5FQzi2odAfdMqRoq-esNeqPuxYNLrOw3Gwi6XXL2x7bi6V9UjBz6SghEkPZYHrCRNBajILvnq8mvpk_8n7_kb7jCbu8v3IYifHB-lJoBfcviE1O0voJRu96Yw8jOEB9E03Wn41i9-UvZOUQubxjvddb46vORCoDl39ha8v3WlJFNk92bSZxj-wUr08px6hmptnCvf8YpNUs25uZ2EI5FW5St5Oxe2Gi8GLZ0PMa00Lt9uAw"
                }
            },
            "request": {
                "type": "IntentRequest",
                "requestId": "amzn1.echo-api.request.44fcb3bc-7ccc-4652-bd11-c8843547bc94",
                "timestamp": "2018-04-22T06:12:29Z",
                "locale": "en-IN",
                "intent": {
                    "name": "CheckOutPatient",
                    "confirmationStatus": "NONE",
                    "slots": {
                        "date": {
                            "name": "date",
                            "confirmationStatus": "NONE"
                        },
                        "followup": {
                            "name": "followup",
                            "confirmationStatus": "NONE"
                        },
                        "PatientFirstName": {
                            "name": "PatientFirstName",
                            "value": "Ali",
                            "confirmationStatus": "NONE"
                        },
                        "gender": {
                            "name": "gender",
                            "confirmationStatus": "NONE"
                        },
                        "PatientLastName": {
                            "name": "PatientLastName",
                            "confirmationStatus": "NONE"
                        },
                        "when": {
                            "name": "when",
                            "confirmationStatus": "NONE"
                        },
                        "age": {
                            "name": "age",
                            "confirmationStatus": "NONE"
                        },
                        "contactno": {
                            "name": "contactno",
                            "confirmationStatus": "NONE"
                        }
                    }
                },
                "dialogState": "STARTED"
            }
        }, ctx)

        ctx.Promise
            .then(resp => { speechResponse = resp; done(); })
            .catch(err => { speechError = err; done(); })
    })

    describe("The response is structurally correct for CheckOutPatient", function() {
        it('should not have errored',function() {
            expect(speechError).to.be.null
        })

        it('should have a version', function() {
            expect(speechResponse.version).not.to.be.null
        })

        it('should have a speechlet response', function() {
            expect(speechResponse.response).not.to.be.null
        })

        it("should have a spoken response", () => {
            expect(speechResponse.response.outputSpeech).not.to.be.null
        })
        it("should have a spoken response ssml", () => {
            expect(speechResponse.response.outputSpeech.ssml).not.to.be.null
        })
        it("should end the alexa session", function() {
            expect(speechResponse.response.shouldEndSession).not.to.be.null
            expect(speechResponse.response.shouldEndSession).to.be.false
        })
    })
})
describe("Testing CancelAppointmentIntent", function() {
    var speechResponse = null
    var speechError = null

    before(function(done){
        index.handler({
            "version": "1.0",
            "session": {
                "new": false,
                "sessionId": "amzn1.echo-api.session.4f1a8646-fcad-4d63-881c-a3628b30aa3d",
                "application": {
                    "applicationId": "amzn1.ask.skill.6ff2c877-21e3-4106-996b-112b78d0b895"
                },
                "attributes": {
                    "STATE": "_STARTMODE"
                },
                "user": {
                    "userId": "amzn1.ask.account.AF5CCYM5ZSJD77M6V5XMUDSEDC5B67NUKEAUIJKB4KKHULPMXYPH7Y2WWWWFZBI327JWUJ4TVGF6MLQRAZKISFX27G4OIW7P2MNQ3RPZZXDI6TJKDCBMEMEP2IFMLPZ5ZQNVIIITBWQ73MED6NVGDXPZO3VC2CENWNGN2EHNQB4RBRH7H4VJZ43ABKHGSSEO4PZEEAUPMNZFZMI"
                }
            },
            "context": {
                "AudioPlayer": {
                    "playerActivity": "IDLE"
                },
                "Display": {
                    "token": ""
                },
                "System": {
                    "application": {
                        "applicationId": "amzn1.ask.skill.6ff2c877-21e3-4106-996b-112b78d0b895"
                    },
                    "user": {
                        "userId": "amzn1.ask.account.AF5CCYM5ZSJD77M6V5XMUDSEDC5B67NUKEAUIJKB4KKHULPMXYPH7Y2WWWWFZBI327JWUJ4TVGF6MLQRAZKISFX27G4OIW7P2MNQ3RPZZXDI6TJKDCBMEMEP2IFMLPZ5ZQNVIIITBWQ73MED6NVGDXPZO3VC2CENWNGN2EHNQB4RBRH7H4VJZ43ABKHGSSEO4PZEEAUPMNZFZMI"
                    },
                    "device": {
                        "deviceId": "amzn1.ask.device.AELI2BPV7TCH6VT4RSYYGTX73XS4XPTZGWKNOOPOO6G27V3WYISPLMVTMWFS7CGDAKPLHGPGDBLG6VHAEKMPMYA2IXWVY5GEL677AP25E2EBWPO4KS4YONWATDD7YAIQOYQFGK3UPEOIDMWOA6GMAJERUYEI6LLBCYZTW5UWY336B4Y7YTPUM",
                        "supportedInterfaces": {
                            "AudioPlayer": {},
                            "Display": {
                                "templateVersion": "1.0",
                                "markupVersion": "1.0"
                            }
                        }
                    },
                    "apiEndpoint": "https://api.eu.amazonalexa.com",
                    "apiAccessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLjZmZjJjODc3LTIxZTMtNDEwNi05OTZiLTExMmI3OGQwYjg5NSIsImV4cCI6MTUyNDM4MTE0OSwiaWF0IjoxNTI0Mzc3NTQ5LCJuYmYiOjE1MjQzNzc1NDksInByaXZhdGVDbGFpbXMiOnsiY29uc2VudFRva2VuIjpudWxsLCJkZXZpY2VJZCI6ImFtem4xLmFzay5kZXZpY2UuQUVMSTJCUFY3VENINlZUNFJTWVlHVFg3M1hTNFhQVFpHV0tOT09QT082RzI3VjNXWUlTUExNVlRNV0ZTN0NHREFLUExIR1BHREJMRzZWSEFFS01QTVlBMklYV1ZZNUdFTDY3N0FQMjVFMkVCV1BPNEtTNFlPTldBVEREN1lBSVFPWVFGR0szVVBFT0lETVdPQTZHTUFKRVJVWUVJNkxMQkNZWlRXNVVXWTMzNkI0WTdZVFBVTSIsInVzZXJJZCI6ImFtem4xLmFzay5hY2NvdW50LkFGNUNDWU01WlNKRDc3TTZWNVhNVURTRURDNUI2N05VS0VBVUlKS0I0S0tIVUxQTVhZUEg3WTJXV1dXRlpCSTMyN0pXVUo0VFZHRjZNTFFSQVpLSVNGWDI3RzRPSVc3UDJNTlEzUlBaWlhESTZUSktEQ0JNRU1FUDJJRk1MUFo1WlFOVklJSVRCV1E3M01FRDZOVkdEWFBaTzNWQzJDRU5XTkdOMkVITlFCNFJCUkg3SDRWSlo0M0FCS0hHU1NFTzRQWkVFQVVQTU5aRlpNSSJ9fQ.BC1aHEnxwIwkSygnDoO5XV_1PGJQwfm0fOsT8zaZf5ATpxL3dXrwXx30o1AXc1BwCJPuHI-dX3yeibaZYSY3hVYg_Dv6jkFNsLUTHwa5FQzi2odAfdMqRoq-esNeqPuxYNLrOw3Gwi6XXL2x7bi6V9UjBz6SghEkPZYHrCRNBajILvnq8mvpk_8n7_kb7jCbu8v3IYifHB-lJoBfcviE1O0voJRu96Yw8jOEB9E03Wn41i9-UvZOUQubxjvddb46vORCoDl39ha8v3WlJFNk92bSZxj-wUr08px6hmptnCvf8YpNUs25uZ2EI5FW5St5Oxe2Gi8GLZ0PMa00Lt9uAw"
                }
            },
            "request": {
                "type": "IntentRequest",
                "requestId": "amzn1.echo-api.request.44fcb3bc-7ccc-4652-bd11-c8843547bc94",
                "timestamp": "2018-04-22T06:12:29Z",
                "locale": "en-IN",
                "intent": {
                    "name": "CancelAppointmentIntent",
                    "confirmationStatus": "NONE",
                    "slots": {
                        "date": {
                            "name": "date",
                            "confirmationStatus": "NONE"
                        },
                        "followup": {
                            "name": "followup",
                            "confirmationStatus": "NONE"
                        },
                        "PatientFirstName": {
                            "name": "PatientFirstName",
                            "value": "Ali",
                            "confirmationStatus": "NONE"
                        },
                        "gender": {
                            "name": "gender",
                            "confirmationStatus": "NONE"
                        },
                        "PatientLastName": {
                            "name": "PatientLastName",
                            "confirmationStatus": "NONE"
                        },
                        "when": {
                            "name": "when",
                            "confirmationStatus": "NONE"
                        },
                        "age": {
                            "name": "age",
                            "confirmationStatus": "NONE"
                        },
                        "contactno": {
                            "name": "contactno",
                            "confirmationStatus": "NONE"
                        }
                    }
                },
                "dialogState": "STARTED"
            }
        }, ctx)

        ctx.Promise
            .then(resp => { speechResponse = resp; done(); })
            .catch(err => { speechError = err; done(); })
    })

    describe("The response is structurally correct for CancelAppointmentIntent", function() {
        it('should not have errored',function() {
            expect(speechError).to.be.null
        })

        it('should have a version', function() {
            expect(speechResponse.version).not.to.be.null
        })

        it('should have a speechlet response', function() {
            expect(speechResponse.response).not.to.be.null
        })

        it("should have a spoken response", () => {
            expect(speechResponse.response.outputSpeech).not.to.be.null
        })
        it("should have a spoken response ssml", () => {
            expect(speechResponse.response.outputSpeech.ssml).not.to.be.null
        })
        it("should end the alexa session", function() {
            expect(speechResponse.response.shouldEndSession).not.to.be.null
            expect(speechResponse.response.shouldEndSession).to.be.false
        })
    })
})
describe("Testing PatientHistory", function() {
    var speechResponse = null
    var speechError = null

    before(function(done){
        index.handler({
            "version": "1.0",
            "session": {
                "new": false,
                "sessionId": "amzn1.echo-api.session.4f1a8646-fcad-4d63-881c-a3628b30aa3d",
                "application": {
                    "applicationId": "amzn1.ask.skill.6ff2c877-21e3-4106-996b-112b78d0b895"
                },
                "attributes": {
                    "STATE": "_STARTMODE"
                },
                "user": {
                    "userId": "amzn1.ask.account.AF5CCYM5ZSJD77M6V5XMUDSEDC5B67NUKEAUIJKB4KKHULPMXYPH7Y2WWWWFZBI327JWUJ4TVGF6MLQRAZKISFX27G4OIW7P2MNQ3RPZZXDI6TJKDCBMEMEP2IFMLPZ5ZQNVIIITBWQ73MED6NVGDXPZO3VC2CENWNGN2EHNQB4RBRH7H4VJZ43ABKHGSSEO4PZEEAUPMNZFZMI"
                }
            },
            "context": {
                "AudioPlayer": {
                    "playerActivity": "IDLE"
                },
                "Display": {
                    "token": ""
                },
                "System": {
                    "application": {
                        "applicationId": "amzn1.ask.skill.6ff2c877-21e3-4106-996b-112b78d0b895"
                    },
                    "user": {
                        "userId": "amzn1.ask.account.AF5CCYM5ZSJD77M6V5XMUDSEDC5B67NUKEAUIJKB4KKHULPMXYPH7Y2WWWWFZBI327JWUJ4TVGF6MLQRAZKISFX27G4OIW7P2MNQ3RPZZXDI6TJKDCBMEMEP2IFMLPZ5ZQNVIIITBWQ73MED6NVGDXPZO3VC2CENWNGN2EHNQB4RBRH7H4VJZ43ABKHGSSEO4PZEEAUPMNZFZMI"
                    },
                    "device": {
                        "deviceId": "amzn1.ask.device.AELI2BPV7TCH6VT4RSYYGTX73XS4XPTZGWKNOOPOO6G27V3WYISPLMVTMWFS7CGDAKPLHGPGDBLG6VHAEKMPMYA2IXWVY5GEL677AP25E2EBWPO4KS4YONWATDD7YAIQOYQFGK3UPEOIDMWOA6GMAJERUYEI6LLBCYZTW5UWY336B4Y7YTPUM",
                        "supportedInterfaces": {
                            "AudioPlayer": {},
                            "Display": {
                                "templateVersion": "1.0",
                                "markupVersion": "1.0"
                            }
                        }
                    },
                    "apiEndpoint": "https://api.eu.amazonalexa.com",
                    "apiAccessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLjZmZjJjODc3LTIxZTMtNDEwNi05OTZiLTExMmI3OGQwYjg5NSIsImV4cCI6MTUyNDM4MTE0OSwiaWF0IjoxNTI0Mzc3NTQ5LCJuYmYiOjE1MjQzNzc1NDksInByaXZhdGVDbGFpbXMiOnsiY29uc2VudFRva2VuIjpudWxsLCJkZXZpY2VJZCI6ImFtem4xLmFzay5kZXZpY2UuQUVMSTJCUFY3VENINlZUNFJTWVlHVFg3M1hTNFhQVFpHV0tOT09QT082RzI3VjNXWUlTUExNVlRNV0ZTN0NHREFLUExIR1BHREJMRzZWSEFFS01QTVlBMklYV1ZZNUdFTDY3N0FQMjVFMkVCV1BPNEtTNFlPTldBVEREN1lBSVFPWVFGR0szVVBFT0lETVdPQTZHTUFKRVJVWUVJNkxMQkNZWlRXNVVXWTMzNkI0WTdZVFBVTSIsInVzZXJJZCI6ImFtem4xLmFzay5hY2NvdW50LkFGNUNDWU01WlNKRDc3TTZWNVhNVURTRURDNUI2N05VS0VBVUlKS0I0S0tIVUxQTVhZUEg3WTJXV1dXRlpCSTMyN0pXVUo0VFZHRjZNTFFSQVpLSVNGWDI3RzRPSVc3UDJNTlEzUlBaWlhESTZUSktEQ0JNRU1FUDJJRk1MUFo1WlFOVklJSVRCV1E3M01FRDZOVkdEWFBaTzNWQzJDRU5XTkdOMkVITlFCNFJCUkg3SDRWSlo0M0FCS0hHU1NFTzRQWkVFQVVQTU5aRlpNSSJ9fQ.BC1aHEnxwIwkSygnDoO5XV_1PGJQwfm0fOsT8zaZf5ATpxL3dXrwXx30o1AXc1BwCJPuHI-dX3yeibaZYSY3hVYg_Dv6jkFNsLUTHwa5FQzi2odAfdMqRoq-esNeqPuxYNLrOw3Gwi6XXL2x7bi6V9UjBz6SghEkPZYHrCRNBajILvnq8mvpk_8n7_kb7jCbu8v3IYifHB-lJoBfcviE1O0voJRu96Yw8jOEB9E03Wn41i9-UvZOUQubxjvddb46vORCoDl39ha8v3WlJFNk92bSZxj-wUr08px6hmptnCvf8YpNUs25uZ2EI5FW5St5Oxe2Gi8GLZ0PMa00Lt9uAw"
                }
            },
            "request": {
                "type": "IntentRequest",
                "requestId": "amzn1.echo-api.request.44fcb3bc-7ccc-4652-bd11-c8843547bc94",
                "timestamp": "2018-04-22T06:12:29Z",
                "locale": "en-IN",
                "intent": {
                    "name": "PatientHistory",
                    "confirmationStatus": "NONE",
                    "slots": {
                        "date": {
                            "name": "date",
                            "confirmationStatus": "NONE"
                        },
                        "followup": {
                            "name": "followup",
                            "confirmationStatus": "NONE"
                        },
                        "PatientFirstName": {
                            "name": "PatientFirstName",
                            "value": "Ali",
                            "confirmationStatus": "NONE"
                        },
                        "gender": {
                            "name": "gender",
                            "confirmationStatus": "NONE"
                        },
                        "PatientLastName": {
                            "name": "PatientLastName",
                            "confirmationStatus": "NONE"
                        },
                        "when": {
                            "name": "when",
                            "confirmationStatus": "NONE"
                        },
                        "age": {
                            "name": "age",
                            "confirmationStatus": "NONE"
                        },
                        "contactno": {
                            "name": "contactno",
                            "confirmationStatus": "NONE"
                        }
                    }
                },
                "dialogState": "STARTED"
            }
        }, ctx)

        ctx.Promise
            .then(resp => { speechResponse = resp; done(); })
            .catch(err => { speechError = err; done(); })
    })

    describe("The response is structurally correct for PatientHistory", function() {
        it('should not have errored',function() {
            expect(speechError).to.be.null
        })

        it('should have a version', function() {
            expect(speechResponse.version).not.to.be.null
        })

        it('should have a speechlet response', function() {
            expect(speechResponse.response).not.to.be.null
        })

        it("should have a spoken response", () => {
            expect(speechResponse.response.outputSpeech).not.to.be.null
        })
        it("should have a spoken response ssml", () => {
            expect(speechResponse.response.outputSpeech.ssml).not.to.be.null
        })
        it("should end the alexa session", function() {
            expect(speechResponse.response.shouldEndSession).not.to.be.null
            expect(speechResponse.response.shouldEndSession).to.be.false
        })
    })
})

describe("Testing GetAppointmentSummary", function() {
    var speechResponse = null
    var speechError = null

    before(function(done){
        index.handler({
            "version": "1.0",
            "session": {
                "new": false,
                "sessionId": "amzn1.echo-api.session.4f1a8646-fcad-4d63-881c-a3628b30aa3d",
                "application": {
                    "applicationId": "amzn1.ask.skill.6ff2c877-21e3-4106-996b-112b78d0b895"
                },
                "attributes": {
                    "STATE": "_STARTMODE"
                },
                "user": {
                    "userId": "amzn1.ask.account.AF5CCYM5ZSJD77M6V5XMUDSEDC5B67NUKEAUIJKB4KKHULPMXYPH7Y2WWWWFZBI327JWUJ4TVGF6MLQRAZKISFX27G4OIW7P2MNQ3RPZZXDI6TJKDCBMEMEP2IFMLPZ5ZQNVIIITBWQ73MED6NVGDXPZO3VC2CENWNGN2EHNQB4RBRH7H4VJZ43ABKHGSSEO4PZEEAUPMNZFZMI"
                }
            },
            "context": {
                "AudioPlayer": {
                    "playerActivity": "IDLE"
                },
                "Display": {
                    "token": ""
                },
                "System": {
                    "application": {
                        "applicationId": "amzn1.ask.skill.6ff2c877-21e3-4106-996b-112b78d0b895"
                    },
                    "user": {
                        "userId": "amzn1.ask.account.AF5CCYM5ZSJD77M6V5XMUDSEDC5B67NUKEAUIJKB4KKHULPMXYPH7Y2WWWWFZBI327JWUJ4TVGF6MLQRAZKISFX27G4OIW7P2MNQ3RPZZXDI6TJKDCBMEMEP2IFMLPZ5ZQNVIIITBWQ73MED6NVGDXPZO3VC2CENWNGN2EHNQB4RBRH7H4VJZ43ABKHGSSEO4PZEEAUPMNZFZMI"
                    },
                    "device": {
                        "deviceId": "amzn1.ask.device.AELI2BPV7TCH6VT4RSYYGTX73XS4XPTZGWKNOOPOO6G27V3WYISPLMVTMWFS7CGDAKPLHGPGDBLG6VHAEKMPMYA2IXWVY5GEL677AP25E2EBWPO4KS4YONWATDD7YAIQOYQFGK3UPEOIDMWOA6GMAJERUYEI6LLBCYZTW5UWY336B4Y7YTPUM",
                        "supportedInterfaces": {
                            "AudioPlayer": {},
                            "Display": {
                                "templateVersion": "1.0",
                                "markupVersion": "1.0"
                            }
                        }
                    },
                    "apiEndpoint": "https://api.eu.amazonalexa.com",
                    "apiAccessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLjZmZjJjODc3LTIxZTMtNDEwNi05OTZiLTExMmI3OGQwYjg5NSIsImV4cCI6MTUyNDM4MTE0OSwiaWF0IjoxNTI0Mzc3NTQ5LCJuYmYiOjE1MjQzNzc1NDksInByaXZhdGVDbGFpbXMiOnsiY29uc2VudFRva2VuIjpudWxsLCJkZXZpY2VJZCI6ImFtem4xLmFzay5kZXZpY2UuQUVMSTJCUFY3VENINlZUNFJTWVlHVFg3M1hTNFhQVFpHV0tOT09QT082RzI3VjNXWUlTUExNVlRNV0ZTN0NHREFLUExIR1BHREJMRzZWSEFFS01QTVlBMklYV1ZZNUdFTDY3N0FQMjVFMkVCV1BPNEtTNFlPTldBVEREN1lBSVFPWVFGR0szVVBFT0lETVdPQTZHTUFKRVJVWUVJNkxMQkNZWlRXNVVXWTMzNkI0WTdZVFBVTSIsInVzZXJJZCI6ImFtem4xLmFzay5hY2NvdW50LkFGNUNDWU01WlNKRDc3TTZWNVhNVURTRURDNUI2N05VS0VBVUlKS0I0S0tIVUxQTVhZUEg3WTJXV1dXRlpCSTMyN0pXVUo0VFZHRjZNTFFSQVpLSVNGWDI3RzRPSVc3UDJNTlEzUlBaWlhESTZUSktEQ0JNRU1FUDJJRk1MUFo1WlFOVklJSVRCV1E3M01FRDZOVkdEWFBaTzNWQzJDRU5XTkdOMkVITlFCNFJCUkg3SDRWSlo0M0FCS0hHU1NFTzRQWkVFQVVQTU5aRlpNSSJ9fQ.BC1aHEnxwIwkSygnDoO5XV_1PGJQwfm0fOsT8zaZf5ATpxL3dXrwXx30o1AXc1BwCJPuHI-dX3yeibaZYSY3hVYg_Dv6jkFNsLUTHwa5FQzi2odAfdMqRoq-esNeqPuxYNLrOw3Gwi6XXL2x7bi6V9UjBz6SghEkPZYHrCRNBajILvnq8mvpk_8n7_kb7jCbu8v3IYifHB-lJoBfcviE1O0voJRu96Yw8jOEB9E03Wn41i9-UvZOUQubxjvddb46vORCoDl39ha8v3WlJFNk92bSZxj-wUr08px6hmptnCvf8YpNUs25uZ2EI5FW5St5Oxe2Gi8GLZ0PMa00Lt9uAw"
                }
            },
            "request": {
                "type": "GetAppointmentSummary",
                "requestId": "amzn1.echo-api.request.44fcb3bc-7ccc-4652-bd11-c8843547bc94",
                "timestamp": "2018-04-22T06:12:29Z",
                "locale": "en-IN",
                "intent": {
                    "name": "PatientHistory",
                    "confirmationStatus": "NONE",
                    "slots": {
                        "date": {
                            "name": "date",
                            "confirmationStatus": "NONE"
                        },
                        "followup": {
                            "name": "followup",
                            "confirmationStatus": "NONE"
                        },
                        "PatientFirstName": {
                            "name": "PatientFirstName",
                            "value": "Ali",
                            "confirmationStatus": "NONE"
                        },
                        "gender": {
                            "name": "gender",
                            "confirmationStatus": "NONE"
                        },
                        "PatientLastName": {
                            "name": "PatientLastName",
                            "confirmationStatus": "NONE"
                        },
                        "when": {
                            "name": "when",
                            "confirmationStatus": "NONE"
                        },
                        "age": {
                            "name": "age",
                            "confirmationStatus": "NONE"
                        },
                        "contactno": {
                            "name": "contactno",
                            "confirmationStatus": "NONE"
                        }
                    }
                },
                "dialogState": "STARTED"
            }
        }, ctx)

        ctx.Promise
            .then(resp => { speechResponse = resp; done(); })
            .catch(err => { speechError = err; done(); })
    })

    describe("The response is structurally correct for GetAppointmentSummary", function() {
        it('should not have errored',function() {
            expect(speechError).to.be.null
        })

        it('should have a version', function() {
            expect(speechResponse.version).not.to.be.null
        })

        it('should have a speechlet response', function() {
            expect(speechResponse.response).not.to.be.null
        })

        it("should have a spoken response", () => {
            expect(speechResponse.response.outputSpeech).not.to.be.null
        })
        it("should have a spoken response ssml", () => {
            expect(speechResponse.response.outputSpeech.ssml).not.to.be.null
        })
        it("should end the alexa session", function() {
            expect(speechResponse.response.shouldEndSession).not.to.be.null
            expect(speechResponse.response.shouldEndSession).to.be.false
        })
    })
})