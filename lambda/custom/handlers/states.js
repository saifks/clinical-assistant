const states = {
    STARTMODE: '_STARTMODE',                // Prompt the user to start or restart the game.
    PRESCIPTIONMODE: '_PRESCIPTIONMODE',                    // Alexa is collecting prescription details.
 //   APPOINTMENTMODE: '_APPOINTMENTMODE'     // Alexa is collecting appointment details
};
module.exports = states;