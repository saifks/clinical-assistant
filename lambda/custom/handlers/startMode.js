
const Alexa = require('alexa-sdk');
const states = require('./states');
const Patient = require('../schemas/patient');
const Appointment = require('../schemas/appointment');
const bookAppointment = require('./functions/bookAppointmet');
const checkin = require('./functions/checkIn');
const checkout = require('./functions/checkout');
const appointmentSummary = require('./functions/appointmentSummary');
const cancelAppointment = require('./functions/cancelAppointment');
const prescription = require('./functions/prescription');
const patientHistory = require('./functions/patientHistory');
const help = require('./functions/help')
var _ = require('lodash');

//var delegateSlotCollection = Helpers.delegateSlotCollection;
//var getSlotValues = Helpers.getSlotValues;
const asyncSeries = require('async/series');


const goodbyeMessage = "Good bye";

module.exports = Alexa.CreateStateHandler(states.STARTMODE, {
    'LaunchRequest': function () {
        //console.log("launching",JSON.stringify(this.event))
        console.log("In start mode launch")
        this.handler.state = states.STARTMODE;
        this.attributes['STATE'] = states.STARTMODE;
        this.response.speak("Welcome to Clinical Assistant,What would you like to do?, Book appointment, check in or check out patient, Prescribe medicine")
        .listen("Sorry I didn't hear anything, did you say something?")
        this.emit(':responseReady');
    },
    'RecordPrescriptionIntent': prescription,
    'BookAppointmentIntent': bookAppointment,
    'CheckOutPatient': checkout,
    'PatientHistory': patientHistory,
    'CheckinPatient': checkin,
    'CancelAppointmentIntent': cancelAppointment,
    'GetAppointmentSummary': appointmentSummary,
    'AMAZON.StopIntent': function () {
        this.response.speak("Stopping now. Thank you");
        this.emit(':responseReady');
    },
    'AMAZON.CancelIntent': function () {
        this.response.speak("Thank you for using clinical assistant");
        this.emit(':responseReady');
    },
    'AMAZON.StartOverIntent': function () {
        this.response.speak("Starting Over again. What Can I do for you?")
        this.emit(':responseReady');
    },
    'AMAZON.HelpIntent': help,
    'Unhandled': function () {
        this.handler.state = states.STARTMODE;
        this.attributes['STATE'] = states.STARTMODE;
        console.log("Inside Unhandled start Mode")

        this.response.speak("Sorry I didn't understand what you want to do. you may book appointment,").listen("Restarting ");
        this.emit(':responseReady');
    }
});
