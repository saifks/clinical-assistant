const startModeHandler = require('./startMode');
const prescriptionHandler = require('./prescriptionMode');
const newSessionHandler = require('./newSession');
module.exports = {
    startModeHandler:startModeHandler,
    prescriptionHandler:prescriptionHandler,
    newSessionHandler:newSessionHandler
}
