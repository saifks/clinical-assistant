
const Alexa = require('alexa-sdk');
const states = require('./states');
const Patient = require('../schemas/patient');
const Appointment = require('../schemas/appointment');
const bookAppointment = require('./functions/bookAppointmet');
const checkin = require('./functions/checkIn');
const checkout = require('./functions/checkout');
const appointmentSummary = require('./functions/appointmentSummary');
const cancelAppointment = require('./functions/cancelAppointment');
const prescription = require('./functions/prescription');
const patientHistory = require('./functions/patientHistory');
var _ = require('lodash');

//var delegateSlotCollection = Helpers.delegateSlotCollection;
//var getSlotValues = Helpers.getSlotValues;
const asyncSeries = require('async/series');


const goodbyeMessage = "Good bye";

module.exports = Alexa.CreateStateHandler(states.PRESCIPTIONMODE, {
    // 'LaunchRequest': function () {
    //     //console.log("launching",JSON.stringify(this.event))
    //     console.log("In prescription mode launch")

    //     this.handler.state = states.STARTMODE;
    //     this.attributes['STATE'] = states.STARTMODE;
    //     this.response.speak("Welcome to Clinical Assistant,").listen("This Clinical assistant..what can I do for you?");
    //     this.emit(':responseReady');
    // },
    'RecordPrescriptionIntent': prescription,
    'BookAppointmentIntent': bookAppointment,
    'CheckOutPatient': checkout,
    'CancelAppointmentIntent': cancelAppointment,
    'GetAppointmentSummary': appointmentSummary,
    'PatientHistory':patientHistory,
    'AMAZON.StopIntent': function () {
        this.response.speak(goodbyeMessage);
        this.emit(':responseReady');
    },
    'AMAZON.CancelIntent': function () {
        this.response.speak(goodbyeMessage);
        this.emit(':responseReady');
    },
    'AMAZON.StartOverIntent': function () {
        this.response.speak("Starting Over again. What Can I do for you?")
        this.emit(':responseReady');
    },
    'AMAZON.HelpIntent': function () {
        this.handler.state = states.STARTMODE;
        this.response.speak("Hi you may book appointment, check in a patient for treatment. You may also record prescriptions for a checked in patient").listen("what do you want to do?");
        this.emit(':responseReady');
    },
    'Unhandled': function () {
        this.handler.state = states.STARTMODE;
        this.attributes['STATE'] = states.STARTMODE;
        console.log("Inside Unhandled Prescription")

        this.response.speak("restarting").listen("Restarting ");
        this.emit(':responseReady');
    }
});
