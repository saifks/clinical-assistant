
const Alexa = require('alexa-sdk');
const Patient = require('../../schemas/patient');
const Appointment = require('../../schemas/appointment');
var moment = require('moment');

module.exports = function () {
    console.log("Inside check In")
    var start = moment().utcOffset("+05:30").startOf('d').toDate();
    //start.setHours(0, 0, 0, 0);

    var end = moment().utcOffset("+05:30").endOf('d').toDate();
    //end.setHours(23, 59, 59, 999);

    var self = this;
    // delegate to Alexa to collect all the required slots
    console.log("Inside checkin", self.attributes['active_appointment'])
    // if (!self.attributes['active_appointment'] || self.attributes['active_appointment'] == undefined || self.attributes['active_appointment'] == "NONE") {
    Appointment
        .scan()
        .where('userId').eq(self.event['session']['user']['userId'])
        .where('status').eq('checked-in')
        .where('dateOfAppointment').between(start, end)
        .exec(function (err, data) {
            if (!err) {
                if(data.Items && data.Items.length > 0){
                    self.response.speak("Already one patient " + data.Items[0].attrs.firstName+" is in treatement, Please check out before you may check in next patient")
                    //.listen('Please say that again?');
                    self.emit(":responseReady");
                }
                else{
                Appointment
                    .scan()
                    .where('userId').eq(self.event['session']['user']['userId'])
                    .where('dateOfAppointment').between(start, end)
                    .where('status').eq('created')
                    .exec(function (err, data) {
                        if (err) {
                            self.response.speak("Some thing went wrong while checking in the patient")
                            self.emit(":responseReady");

                            console.log("patient check in  error ", err)
                        }
                        else {
                            if (data.Items && data.Items.length > 0) {
                                // console.log(data);
                                // console.log(data.Items);
                                // console.log(data.Items[0].attrs);
                                //var data1 =  _.pluck(data.Items, 'attrs');

                                Appointment.update({ appointmentId: data.Items[0].attrs.appointmentId, userId: data.Items[0].attrs.userId, status: "checked-in" }, function (err, acc) {
                                    if (!err) {
                                        self.attributes['active_appointment'] = data.Items[0].attrs.appointmentId;

                                        self.response.speak("Checked In Patient " + data.Items[0].attrs.firstName)
                                           // .listen("Any more instructions for me?");

                                        self.emit(":responseReady");
                                    }
                                    else {
                                        self.response.speak("Some thing went wrong while checking in the patient")
                                        self.emit(":responseReady");
                                        console.log(err)
                                    }
                                });
                                console.log("Patient Found ", data.Items[0].attrs)

                            }
                            else {
                                console.log("Patient Not Found", data)
                                self.response.speak("no More Appointments today")
                                    //.listen("Any more instructions for me?");
                                self.emit(':responseReady');
                            }
                        }
                    });
                }
            }
            else {
                //self.attributes['question'] = "check-out";
                self.response.speak("Some thing went wrong while checking in the patient")
                self.emit(":responseReady");
                console.log(err)
                
            }
        })
}
