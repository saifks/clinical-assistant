
const Alexa = require('alexa-sdk');
const states = require('../states');
const Helpers = require('./helpers');
const Patient = require('../../schemas/patient');
const Appointment = require('../../schemas/appointment');
var _ = require('lodash');
var moment = require('moment');
var delegateSlotCollection = Helpers.delegateSlotCollection;
var getSlotValues = Helpers.getSlotValues;
const asyncSeries = require('async/series');


module.exports = function () {
    // delegate to Alexa to collect all the required slots
    // console.log(JSON.stringify(this.request))
    let filledSlots = delegateSlotCollection.call(this);

    if (!filledSlots) {
        return;
    }

    console.log("filled slots: " + JSON.stringify(filledSlots));
    // at this point, we know that all required slots are filled.
    let slotValues = getSlotValues(filledSlots);

    console.log(JSON.stringify(slotValues));
    var self = this;
    Patient
        .query(slotValues.contactno.resolved)
        .usingIndex('phone-userId-index')
        .where('userId').eq(self.event['session']['user']['userId'])
        .exec(function (err, data) {
            if (err) {
                console.log("patient creation error ", err)
            }
            else {
                if (data.Items && data.Items.length > 0) {
                    // console.log(data);
                    // console.log(data.Items);
                    // console.log(data.Items[0].attrs);
                    //var data1 =  _.pluck(data.Items, 'attrs');

                    console.log("Patient Found ", data.Items[0].attrs)
                    Appointment.create({
                        status: "created",
                        patientId: data.Items[0].attrs.patientId,
                        firstName: slotValues.PatientFirstName.resolved,
                        lastName: slotValues.PatientLastName.resolved,
                        followUp: slotValues.followup.resolved == "Yes" ? true : false,
                        expectedStartTime: slotValues.when.resolved,//slotValues.when.resolved
                        dateOfAppointment: moment(slotValues.date.resolved).utcOffset("+05:30").startOf('d').toDate(),
                        userId: self.event['session']['user']['userId'],
                        prescriptions: []
                    }, function (err, res) {
                        if (!err) {
                            console.log(res.attrs)
                            // self.response.speak("Patient is created " + slotValues.PatientFirstName.resolved);
                            var day = moment(slotValues.date.resolved).utcOffset("+05:30").calendar().split(/ /)

                            self.response.speak("Appointment is Booked for " + slotValues.PatientFirstName.resolved + " for " + day[0] + " at " + slotValues.when.resolved)
                            .cardRenderer("Appointment", "Appointment is Booked for " + slotValues.PatientFirstName.resolved + " for " + day[0] + " at " + slotValues.when.resolved);

                            self.emit(":responseReady");
                        }
                        else {
                            console.log("error", err)

                        }
                    })
                }
                else {
                    console.log("Patient Not Found", data)
                    Patient.create({
                        firstName: slotValues.PatientFirstName.resolved,
                        lastName: slotValues.PatientLastName.resolved,
                        age: slotValues.age.resolved,
                        gender: slotValues.gender.resolved,
                        phone: slotValues.contactno.resolved,
                        userId: self.event['session']['user']['userId']

                    }, function (err, res) {
                        if (err) {
                            console.log(err)

                        }
                        else {
                            Appointment.create({
                                status: "created",
                                patientId: res.get().patientId,
                                firstName: slotValues.PatientFirstName.resolved,
                                lastName: slotValues.PatientLastName.resolved,
                                followUp: slotValues.followup.resolved == "Yes" ? true : false,
                                expectedStartTime: slotValues.when.resolved,//slotValues.when.resolved
                                dateOfAppointment: moment(slotValues.date.resolved).utcOffset("+05:30").startOf('d').toDate(),
                                userId: self.event['session']['user']['userId']
                            }, function (err, res) {
                                if (!err) {
                                    console.log(res)
                                    // self.response.speak("Patient is created " + slotValues.PatientFirstName.resolved);
                                    var day = moment(slotValues.date.resolved).utcOffset("+05:30").calendar().split(/ /)
                                    self.response.speak("Appointment is Booked for " + slotValues.PatientFirstName.resolved + " for " + day[0] + " at " + slotValues.when.resolved);
                                    self.emit(":responseReady");
                                }
                                else {
                                    self.response.speak("Some thing went wrong while booking appointment")
                                    self.emit(":responseReady");
                                    console.log("error", err)
                                }
                            })

                            // self.emit(":responseReady");
                            // console.log(res.get())
                        }
                    })

                }
            }
        });

    //let speechOutput = "Booking Appointment for "+slotValues.PatientName.resolved+" as "+slotValues.followup.resolved;

    //console.log("Speech output: ", speechOutput);
    //this.response.speak(speechOutput);
    //this.emit(":responseReady");

}

