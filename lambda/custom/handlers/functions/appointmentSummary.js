
const Alexa = require('alexa-sdk');
const states = require('../states');
const Helpers = require('./helpers');
const Patient = require('../../schemas/patient');
const Appointment = require('../../schemas/appointment');
var _ = require('lodash');
const moment = require('moment');

var delegateSlotCollection = Helpers.delegateSlotCollection;
var getSlotValues = Helpers.getSlotValues;
const asyncSeries = require('async/series');


module.exports = function () {
    let filledSlots = delegateSlotCollection.call(this);

    if (!filledSlots) {
        return;
    }

    console.log("filled slots: " + JSON.stringify(filledSlots));
    // at this point, we know that all required slots are filled.
    let slotValues = getSlotValues(filledSlots);

    console.log(JSON.stringify(slotValues));
    var self = this;
    console.log("Inside Appointment Summary")
    if (slotValues.Day.resolved) {
        var start = moment(slotValues.Day.resolved).utcOffset("+05:30").startOf('d').toDate();
        //start.setHours(0, 0, 0, 0);

        var end = moment(slotValues.Day.resolved).utcOffset("+05:30").endOf('d').toDate();
        // var start = new Date(slotValues.day.resolved);
        // start.setHours(0, 0, 0, 0);

        // var end = new Date(slotValues.day.resolved);
        // end.setHours(23, 59, 59, 999);
    }
    else {
        var start = moment(slotValues.Day.resolved).utcOffset("+05:30").startOf('d').toDate();
        var end = moment(slotValues.Day.resolved).utcOffset("+05:30").endOf('d').toDate();
        // var start = new Date();
        // start.setHours(0, 0, 0, 0);

        // var end = new Date();
        // end.setHours(23, 59, 59, 999);
    }


    var self = this;
    Appointment
        .scan()
        .where('userId').eq(self.event['session']['user']['userId'])
        .where('dateOfAppointment').between(start, end)
        .exec(function (err, data) {
            if (err) {
                console.log("patient check in  error ", err)
                self.response.speak("Something went wrong")
                self.emit(':responseReady');
            }
            else {
                if (data.Items && data.Items.length > 0) {
                    var day = moment(slotValues.Day.resolved).utcOffset("+05:30").calendar().split(/ /)
                    self.response.speak("You Have " + data.Items.length + " appointments " + day[0])
                    self.emit(":responseReady");

                }
                else {
                    console.log("Patient Not Found", data)
                    var day = moment(slotValues.Day.resolved).utcOffset("+05:30").calendar().split(/ /)

                    self.response.speak("No Appointments " + day[0])
                    self.emit(':responseReady');
                }
            }
        });


    // ask the first question

}


