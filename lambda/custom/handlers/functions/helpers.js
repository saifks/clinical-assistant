// ***********************************
// ** Helper functions from
// ** These should not need to be edited
// ** www.github.com/alexa/alexa-cookbook
// ***********************************

// ***********************************
// ** Dialog Management
// ***********************************
const Patient = require('../../schemas/patient');

function getSlotValues(filledSlots) {
    //given event.request.intent.slots, a slots values object so you have
    //what synonym the person said - .synonym
    //what that resolved to - .resolved
    //and if it's a word that is in your slot values - .isValidated
    let slotValues = {};

    console.log("The filled slots: " + JSON.stringify(filledSlots));
    Object.keys(filledSlots).forEach(function (item) {

        // console.log("item in filledSlots: "+JSON.stringify(filledSlots[item]));

        let name = filledSlots[item].name;
        //console.log("name: "+name);

        if (filledSlots[item] &&
             filledSlots[item].resolutions &&
             filledSlots[item].resolutions.resolutionsPerAuthority[0] &&
             filledSlots[item].resolutions.resolutionsPerAuthority[0].status &&
             filledSlots[item].resolutions.resolutionsPerAuthority[0].status.code) {

            switch (filledSlots[item].resolutions.resolutionsPerAuthority[0].status.code) {
            case "ER_SUCCESS_MATCH":
                slotValues[name] = {
                    "synonym": filledSlots[item].value,
                    "resolved": filledSlots[item].resolutions.resolutionsPerAuthority[0].values[0].value.name,
                    "isValidated": true
                };
                break;
            case "ER_SUCCESS_NO_MATCH":
                slotValues[name] = {
                    "synonym": filledSlots[item].value,
                    "resolved": filledSlots[item].value,
                    "isValidated":false
                };
                break;
            }
        } else {
            slotValues[name] = {
                "synonym": filledSlots[item].value,
                "resolved": filledSlots[item].value,
                "isValidated": false
            };
        }
    },this);

    //console.log("slot values: "+JSON.stringify(slotValues));
    return slotValues;
}

// This function delegates multi-turn dialogs to Alexa.
// For more information about dialog directives see the link below.
// https://developer.amazon.com/docs/custom-skills/dialog-interface-reference.html
function delegateSlotCollection() {
    console.log("in delegateSlotCollection");
   // console.log(this.event)
    console.log("current dialogState: " + this.event.request.dialogState);

    if (this.event.request.dialogState === "STARTED") {
        console.log("in STARTED");
        //console.log(JSON.stringify(this.event));
        let updatedIntent = this.event.request.intent;
        // optionally pre-fill slots: update the intent object with slot values
        // for which you have defaults, then return Dialog.Delegate with this
        // updated intent in the updatedIntent property

        if(updatedIntent.name == "BookAppointmentIntent" && updatedIntent.slots.contactno.value && (!updatedIntent.slots.PatientLastName.value || !updatedIntent.slots.gender.value)){
            var self =this;
            Patient
            .query(updatedIntent.slots.contactno.value)
            .usingIndex('phone-userId-index')
            .where('userId').eq(this .event['session']['user']['userId'])
            .exec(function (err, data) {
                if(!err){
                    if (data.Items && data.Items.length > 0) {
                        // console.log(data);
                        // console.log(data.Items);
                        // console.log(data.Items[0].attrs);
                         //var data1 =  _.pluck(data.Items, 'attrs');
                         updatedIntent.slots.PatientLastName = {
                            "name": "PatientLastName",
                            "value": data.Items[0].attrs.lastName,
                            "confirmationStatus": "NONE"
                         }
                         updatedIntent.slots.PatientFirstName = {
                            "name": "PatientLastName",
                            "value": data.Items[0].attrs.firstName,
                            "confirmationStatus": "NONE"
                         }
                         updatedIntent.slots.age = {
                            "name": "age",
                            "value": data.Items[0].attrs.age,
                            "confirmationStatus": "NONE"
                         }
                         updatedIntent.slots.gender = {
                            "name": "gender",
                            "value": data.Items[0].attrs.gender,
                            "confirmationStatus": "NONE"
                         }
                         disambiguateSlot.call(self);

                         console.log("Patient Found ",data.Items[0].attrs)
                         self.emit(":delegate", updatedIntent);

                    }
                    else{
                        disambiguateSlot.call(self);

                       // console.log("Patient Found ",data.Items[0].attrs)
                        self.emit(":delegate", updatedIntent);
                    }
                }
            })
        }
        else{
            disambiguateSlot.call(this);

        //console.log("disambiguated: " + JSON.stringify(this.event));
        this.emit(":delegate", updatedIntent);
        }
    } else if (this.event.request.dialogState !== "COMPLETED") {
        console.log("in not completed");
        let updatedIntent = this.event.request.intent;
        //console.log(JSON.stringify(this.event));


        if(updatedIntent.name == "BookAppointmentIntent" && updatedIntent.slots.contactno.value && (!updatedIntent.slots.PatientLastName.value || !updatedIntent.slots.gender.value)){
            var self =this;
            Patient
            .query(updatedIntent.slots.contactno.value)
            .usingIndex('phone-userId-index')
            .where('userId').eq(this.event['session']['user']['userId'])
            .exec(function (err, data) {
                if(!err){
                    if (data.Items && data.Items.length > 0) {
                        // console.log(data);
                        // console.log(data.Items);
                        // console.log(data.Items[0].attrs);
                         //var data1 =  _.pluck(data.Items, 'attrs');
                         updatedIntent.slots.PatientLastName = {
                            "name": "PatientLastName",
                            "value": data.Items[0].attrs.lastName,
                            "confirmationStatus": "NONE"
                         }
                         updatedIntent.slots.PatientFirstName = {
                            "name": "PatientFirstName",
                            "value": data.Items[0].attrs.firstName,
                            "confirmationStatus": "NONE"
                         }
                         updatedIntent.slots.age = {
                            "name": "age",
                            "value": data.Items[0].attrs.age,
                            "confirmationStatus": "NONE"
                         }
                         updatedIntent.slots.gender = {
                            "name": "gender",
                            "value": data.Items[0].attrs.gender,
                            "confirmationStatus": "NONE"
                         }
                         
                         disambiguateSlot.call(self);

                         //console.log("Patient Found ",data.Items[0].attrs)
                         self.emit(":delegate", updatedIntent);
                    }
                    else{
                        disambiguateSlot.call(self);

                        //console.log("Patient Found ",data.Items[0].attrs)
                        self.emit(":delegate", updatedIntent);
                    }
                }
            })
        }
        else{
            disambiguateSlot.call(this);

       // console.log("disambiguated: " + JSON.stringify(this.event));
        this.emit(":delegate", updatedIntent);
        }
    } else {
        console.log("in completed");
        //console.log("returning: "+ JSON.stringify(this.event.request.intent));
        // Dialog is now complete and all required slots should be filled,
        // so call your normal intent handler.
        return this.event.request.intent.slots;
    }
    return null;
}

// If the user said a synonym that maps to more than one value, we need to ask
// the user for clarification. Disambiguate slot will loop through all slots and
// elicit confirmation for the first slot it sees that resolves to more than
// one value.
function disambiguateSlot() {
    let currentIntent = this.event.request.intent;
    let prompt = "";
    Object.keys(this.event.request.intent.slots).forEach(function (slotName) {
        let currentSlot = currentIntent.slots[slotName];
        // let slotValue = slotHasValue(this.event.request, currentSlot.name);
        if (currentSlot.confirmationStatus !== "CONFIRMED" &&
            currentSlot.resolutions &&
            currentSlot.resolutions.resolutionsPerAuthority[0]) {

            if (currentSlot.resolutions.resolutionsPerAuthority[0].status.code === "ER_SUCCESS_MATCH") {
                // if there's more than one value that means we have a synonym that
                // mapped to more than one value. So we need to ask the user for
                // clarification. For example if the user said "mini dog", and
                // "mini" is a synonym for both "small" and "tiny" then ask "Did you
                // want a small or tiny dog?" to get the user to tell you
                // specifically what type mini dog (small mini or tiny mini).
                if (currentSlot.resolutions.resolutionsPerAuthority[0].values.length > 1) {
                    prompt = "Which would you like";
                    let size = currentSlot.resolutions.resolutionsPerAuthority[0].values.length;
                    currentSlot.resolutions.resolutionsPerAuthority[0].values.forEach(function (element, index, arr) {
                        prompt += ` ${(index === size - 1) ? " or" : " "} ${element.value.name}`;
                    });

                    prompt += "?";
                    let reprompt = prompt;
                    // In this case we need to disambiguate the value that they
                    // provided to us because it resolved to more than one thing so
                    // we build up our prompts and then emit elicitSlot.
                    this.emit(":elicitSlot", currentSlot.name, prompt, reprompt);
                }
            } else if (currentSlot.resolutions.resolutionsPerAuthority[0].status.code === "ER_SUCCESS_NO_MATCH") {
                // Here is where you'll want to add instrumentation to your code
                // so you can capture synonyms that you haven't defined.
                console.log("NO MATCH FOR: ", currentSlot.name, " value: ", currentSlot.value);

                //if (REQUIRED_SLOTS.indexOf(currentSlot.name) > -1) {
                    prompt = "What " + currentSlot.name + " are you looking for";
                    this.emit(":elicitSlot", currentSlot.name, prompt, prompt);
                //}
            }
        }
    }, this);
}

// Given the request an slot name, slotHasValue returns the slot value if one
// was given for `slotName`. Otherwise returns false.
function slotHasValue(request, slotName) {

    let slot = request.intent.slots[slotName];

    // uncomment if you want to see the request
    // console.log("request = "+JSON.stringify(request));
    let slotValue;

    // if we have a slot, get the text and store it into speechOutput
    if (slot && slot.value) {
        // we have a value in the slot
        slotValue = slot.value.toLowerCase();
        return slotValue;
    } else {
        // we didn't get a value in the slot.
        return false;
    }
}
module.exports = {
    slotHasValue:slotHasValue,
    disambiguateSlot:disambiguateSlot,
    delegateSlotCollection:delegateSlotCollection,
    getSlotValues:getSlotValues
}