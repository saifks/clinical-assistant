
const Alexa = require('alexa-sdk');
const Patient = require('../../schemas/patient');
const Appointment = require('../../schemas/appointment');
var _ = require('lodash');
var moment = require('moment');

module.exports = function () {
    console.log("Inside patient History")

    var start = moment().utcOffset("+05:30").startOf('d').toDate();
    //start.setHours(0, 0, 0, 0);

    var end = moment().utcOffset("+05:30").endOf('d').toDate();
    var self = this;
    Appointment
        .scan()
        .where('userId').eq(self.event['session']['user']['userId'])
        .where('status').eq('checked-in')
        .where('dateOfAppointment').between(start, end)
        .exec(function (err, data) {
            if (err) {
                console.log("patient check out  error ", err)
            }
            else {
                if (data.Items && data.Items.length > 0) {
                    // console.log(data);
                    // console.log(data.Items);
                    // console.log(data.Items[0].attrs);
                    //var data1 =  _.pluck(data.Items, 'attrs');
                    Appointment.scan()
                        .where('patientId').eq(data.Items[0].attrs.patientId)
                        .where('status').eq('checked-out')
                        .exec(function (err, acc) {
                            if (!err) {
                                //console.lo
                                if (acc.Items && acc.Items.length > 0 && acc.Items[0].attrs && acc.Items[0].attrs.prescriptions) {
                                    var speach = "Patient Name is "+acc.Items[0].attrs.firstName +", During last visit you prescribed ";

                                    _.forEach(acc.Items[0].attrs.prescriptions, function (thisItem, index) {
                                        var append = "";
                                        if (index == acc.Items.length) {
                                            append = " and "
                                        }
                                        console.log("this Item", thisItem)
                                        speach = speach + ", " + append + thisItem.medicineName + " "
                                            + thisItem.dosage + " milli grams " + thisItem.frequency +
                                            " time a day for " + thisItem.duration + " days"
                                    })
                                    console.log(speach)
                                    self.response.speak(speach)
                                    .cardRenderer("Patient Summary", speach);

                                      //  .listen("Any more instructions for me?");

                                    self.emit(":responseReady");
                                }
                                else {
                                    self.response.speak("No History available for patient")
                                      ///  .listen("Any more instructions for me?");

                                    self.emit(":responseReady");
                                }
                            }
                            else {
                                self.response.speak("Something went wrong during check out ")
                                   // .listen("Any more instructions for me?");

                                self.emit(":responseReady");
                            }
                        });

                }
                else {
                    console.log("Appointme Not Found", data)
                    self.response.speak("No Patient is currently checked in")
                    // .listen("Any more instructions for me?");

                    self.emit(':responseReady');
                }
            }
        });


    // ask the first question

}
