
const Alexa = require('alexa-sdk');
const Patient = require('../../schemas/patient');
const Appointment = require('../../schemas/appointment');
const Helpers = require('./helpers');
var delegateSlotCollection = Helpers.delegateSlotCollection;
var getSlotValues = Helpers.getSlotValues;
const cancelAppointment = function () {
    console.log("Inside Cancel Appointment")

    let filledSlots = delegateSlotCollection.call(this);

    if (!filledSlots) {
        return;
    }

    console.log("filled slots: " + JSON.stringify(filledSlots));
    // at this point, we know that all required slots are filled.
    let slotValues = getSlotValues(filledSlots);

    console.log(JSON.stringify(slotValues));
    var self = this;
    Patient
        .query(slotValues.contactno.resolved)
        .usingIndex('phone-userId-index')
        .where('userId').eq(self.event['session']['user']['userId'])
        .exec(function (err, data) {
            if (err) {
                console.log("patient creation error ", err)
            }
            else {
                if (data.Items && data.Items.length > 0) {
                    // console.log(data);
                    // console.log(data.Items);
                    // console.log(data.Items[0].attrs);
                    //var data1 =  _.pluck(data.Items, 'attrs');

                    console.log("Patient Found ", data.Items[0].attrs)
                    Appointment
                        .scan()
                        .where('patientId').eq(data.Items[0].attrs.patientId)
                        .where('status').eq('created')
                        .exec(function (err, res) {
                            if (!err) {
                                if (res.Items && res.Items.length > 0) {

                                    Appointment.update({ appointmentId: data.Items[0].attrs.appointmentId,userId:data.Items[0].attrs.userId , status: "cancelled" }, function (err, acc) {
                                        self.response.speak("Appointment cancelled for patient " + data.Items[0].attrs.firstName)
                                    
                                        self.emit(":responseReady");
                                    });

                                }
                                else {
                                    self.response.speak("No Appointment found for the patient")

                                    self.emit(":responseReady");
                                }
                                // self.response.speak("Patient is created " + slotValues.PatientFirstName.resolved);

                            }
                            else {
                                console.log("error", err)

                            }
                        })
                }
                else {
                    console.log("Patient Not Found", data)
                    self.response.speak("No patient is found with the given contact info")
                    
                    self.emit(":responseReady");

                }
            }
        });

}
module.exports = cancelAppointment;