
const states = require('../states');
const Alexa = require('alexa-sdk');
const Helpers = require('./helpers');
var delegateSlotCollection = Helpers.delegateSlotCollection;
var getSlotValues = Helpers.getSlotValues;
const Joi = require('joi');
var moment = require('moment');

module.exports = function () {
    var self = this;
    // delegate to Alexa to collect all the required slots
    console.log("Inside Prescription")
    var start = moment().utcOffset("+05:30").startOf('d').toDate();
    //start.setHours(0, 0, 0, 0);

    var end = moment().utcOffset("+05:30").endOf('d').toDate();
    Appointment
        .scan()
        .where('userId').eq(self.event['session']['user']['userId'])
        .where('status').eq('checked-in')
        .where('dateOfAppointment').between(start, end)
        .exec(function (err, data) {
            if (!err) {
                if (data.Items && data.Items.length > 0) {
                    let filledSlots = delegateSlotCollection.call(self);

                    if (!filledSlots) {
                        return;
                    }
                    console.log("filled slots: " + JSON.stringify(filledSlots));
                    // at this point, we know that all required slots are filled.
                    let slotValues = getSlotValues(filledSlots);

                    console.log(JSON.stringify(slotValues));
                    //self.response.speak("Already one patient " + data.Items[0].attrs.firstName + " is in treatement, Please check out before you may check in next patient")
                    //.listen('Please say that again?');
                    //self.emit(":responseReady");
                    var pres = data.Items[0].attrs.prescriptions ? data.Items[0].attrs.prescriptions : [];
                    pres.push({
                        medicineName: slotValues.medicine.resolved,
                        dosage: slotValues.dosage.resolved,
                        duration: slotValues.duration.resolved,// #number of days
                        frequency: slotValues.frequency.resolved, //#times a day
                        specialInstructions: slotValues.specialInstructions.resolved ? slotValues.specialInstructions.resolved : "none"
                    })
                    console.log("new pres", pres)
                    Appointment.update({
                        appointmentId: data.Items[0].attrs.appointmentId,
                        userId: self.event['session']['user']['userId'],
                        prescriptions: pres
                    }, function (err, acc) {
                        if (!err) {
                            console.log("slot vals", JSON.stringify(slotValues));
                            var dose = slotValues.dosage.resolved
                            dose = dose.replace(/^0+/, '');
                            //self.attributes['active_appointment'] = data.Items[0].attrs.appointmentId;
                            var speechOutput = "Prescription Added, " + slotValues.medicine.resolved +
                                "  " + dose + " milli grams " +
                                slotValues.frequency.resolved +
                                " times a day for " + slotValues.duration.resolved +
                                " days " //+
                            //" special Instructions " + slotValues.specialInstructions.resolved? slotValues.specialInstructions.resolved: "none."
                            console.log(speechOutput)
                            self.response.speak(speechOutput)
                            .cardRenderer("Prescription", speechOutput);
                            self.emit(":responseReady");
                        }
                        else {
                            console.log("update", err)
                            self.response.speak("Some thing went wrong while saving prescription")
                            self.emit(":responseReady");
                        }
                    });
                }
                else {
                    self.response.speak("No Patient Is Checked In currently, Please check in a patient before starting treatment")
                    self.emit(":responseReady");
                }
            }
            else {
                self.response.speak("Some thing went wrong while noting the prescription")
                self.emit(":responseReady");
                console.log(err)
            }
        })
}
