
const Alexa = require('alexa-sdk');
const Patient = require('../../schemas/patient');
const Appointment = require('../../schemas/appointment');
var moment = require('moment');

const checkout = function () {
    console.log("Inside check Out")

    var start = moment().utcOffset("+05:30").startOf('d').toDate();
    //start.setHours(0, 0, 0, 0);

    var end = moment().utcOffset("+05:30").endOf('d').toDate();

    var self = this;
    Appointment
        .scan()
        .where('userId').eq(self.event['session']['user']['userId'])
        .where('status').eq('checked-in')
        .where('dateOfAppointment').between(start, end)
        .exec(function (err, data) {
            if (err) {
                console.log("patient check out  error ", err)
            }
            else {
                if (data.Items && data.Items.length > 0) {
                    // console.log(data);
                    // console.log(data.Items);
                    // console.log(data.Items[0].attrs);
                    //var data1 =  _.pluck(data.Items, 'attrs');
                    Appointment.update({ appointmentId: data.Items[0].attrs.appointmentId, userId:data.Items[0].attrs.userId ,status: "checked-out" }, function (err, acc) {
                        if (!err) {
                            console.log(acc)
                            self.attributes['active_appointment'] = "NONE";
                            self.response.speak("Checked Out Patient " + data.Items[0].attrs.firstName + ", Say check in to check in next patient")
                            .listen("Please say that again");
                            self.emit(":responseReady");
                        }
                        else {
                            self.response.speak("Something went wrong during check out ")
                            //.listen("Any more instructions for me?");
                            
                            self.emit(":responseReady");
                        }
                    });

                }
                else {
                    console.log("Appointme Not Found", data)
                    self.response.speak("No Patient is currently checked in")
                    //.listen("Any more instructions for me?");
                    
                    self.emit(':responseReady');
                }
            }
        });


    // ask the first question

}
module.exports = checkout;