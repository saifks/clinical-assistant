
const Alexa = require('alexa-sdk');
const states = require('./states');
const Patient = require('../schemas/patient');
const Appointment = require('../schemas/appointment');
const bookAppointment = require('./functions/bookAppointmet');
const checkin = require('./functions/checkIn');
const checkout = require('./functions/checkout');
const appointmentSummary = require('./functions/appointmentSummary');
const cancelAppointment = require('./functions/cancelAppointment');
var _ = require('lodash');

//var delegateSlotCollection = Helpers.delegateSlotCollection;
//var getSlotValues = Helpers.getSlotValues;
const asyncSeries = require('async/series');


const goodbyeMessage = "Good bye";

const newSessionHandler = Alexa.CreateStateHandler(null, {
    'LaunchRequest': function () {
       // console.log("launching",JSON.stringify(this.event))
       console.log("In new session launch")

        this.handler.state = states.STARTMODE;
        this.attributes['STATE'] = states.STARTMODE;
        this.response.speak("Welcome to Clinical Assistant,").listen("This Clinical assistant..what can I do for you?");
        this.emit(':responseReady');
    },
    'Unhandled': function () {
        this.handler.state = states.STARTMODE;
        this.attributes['STATE'] = states.STARTMODE;
        console.log("Inside Unhandled new Session")
        this.response.speak("restarting").listen("Restarting ");
        this.emit(':responseReady');
    }
});
module.exports = newSessionHandler;