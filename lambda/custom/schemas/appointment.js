
//const AWS = require("aws-sdk");

var dynamo = require('dynamodb');
const Joi = require('joi');

dynamo.AWS.config.update({region: "ap-south-1"});
//dynamo.AWS.config.update({region: "us-east-1"});
//var dynamodb = new AWS.DynamoDB();

const Prescription = Joi.object({
   // medicineId: Joi.string().allow(null), //unique Id of medicine
    medicineName: Joi.string(), // brandName
    dosage: Joi.string(), // dosage in english
   // dosageCode: Joi.string().allow(null), // dosage code for eg: 1-0-1, 1-1-1 etc
    duration: Joi.string(),// #number of days
    frequency:Joi.string(), //#times a day
    specialInstructions:Joi.string().allow(null)
})
const TreatmentNote = Joi.object({
    //symptoms:Joi.array().items(Joi.string()),
    //diagnostics:Joi.array().items(Joi.string()),
    //findings:Joi.array().items(Joi.string()),
    prescriptions: Joi.array().items(Prescription),
})
Appointment = dynamo.define('Appointment', {
    hashKey: 'appointmentId',
    rangeKey: 'userId',
    timestamps: true,
    tableName: 'HackathonAppointments2',
    schema: {
        //email   : Joi.string().email(),
        firstName: Joi.string().allow(null),
        lastName: Joi.string().allow(null),
        patientId: Joi.string(),
        expectedStartTime: Joi.string(),
        dateOfAppointment: Joi.date(),
        actualStartTime: Joi.date().timestamp().allow(null),
        followUp: Joi.boolean(),
        status: Joi.string().valid("created", "checked-in", "checked-out", "cancelled", "no-show").allow(null),
        statusHistory: Joi.array().items([Joi.object({
            status: Joi.string().valid("created", "checked-in", "checked-out", "cancelled", "no-show"),
            effectiveTime: Joi.date().timestamp(),
        })]).allow(null),
        appointmentId: dynamo.types.uuid(),
        userId: Joi.string(),
        //symptoms:Joi.array().items(Joi.string()),
        //diagnostics:Joi.array().items(Joi.string()),
        //findings:Joi.array().items(Joi.string()),
        //prescriptions: //Joi.array().items([Joi.object()]).allow(null),
        prescriptions : Joi.array().items(Joi.object().keys({
            medicineName : Joi.string(),
            dosage  : Joi.string(),
            duration: Joi.string(),// #number of days
            frequency:Joi.string(), //#times a day
            specialInstructions:Joi.string().allow(null)
          }))
    }
});
Appointment.prototype.createNew = function (params) {
    Appointment.create({
        status: "created",
        patientId: params.patientId,
        firstName: params.firstName,
        lastName: params.lastName,
        followUp: params.followUp ? true : false,
        expectedStartTime: params.expectedStartTime
    }, function (err, acc) {
        if (err) {
            console.log('Error: Creating Appointment', param)
        }
        else {
            console.log('Success: Created Appointment', param)
        }
    });
}
Appointment.prototype.setStatus = function (params) {
    var obj = {
        appoitmentId: params.appoitmentId,
        status: param.status,
        //actualStartTime:params.actualStartTime,
        statusHistory: {
            $add: {
                status: param.status,
                effectiveTime: new Date()
            }
        }
    }
    if (params.status = 'checked-in') {
        obj.actualStartTime = new Date();
    }
    Appointment.update(obj, function (err, acc) {
        if (err) {
            console.log('Error: Updating appointment status ', obj)
        }
        else {
            onsole.log('Success: Updated appointment status ', obj)
        }
    });
}
Appointment.prototype.cancel = function (params) {
    var obj = {
        appoitmentId: params.appoitmentId,
        status: "cancelled",
        //actualStartTime:params.actualStartTime,
        statusHistory: {
            $add: {
                status: "cancelled",
                effectiveTime: new Date()
            }
        }
    }
    Appointment.update(obj, function (err, acc) {
        if (err) {
            console.log('Error: Updating appointment status ', obj)
        }
        else {
            onsole.log('Success: Updated appointment status ', obj)
        }
    });
}
Appointment.prototype.addPrescription = function (params) {
    Appointment.update({
        appoitmentId: params.appoitmentId,
        prescriptions: {
            $add: paramsprescription
        }
    }, function (err, acc) {
        if (err) {
            console.log('Error: Adding prescription', param)
        }
        else {
            onsole.log('Success: Added prescription', param)
        }
    });
}

module.exports = Appointment;