//const AWS = require("aws-sdk");

var dynamo = require('dynamodb');
const Joi = require('joi');
dynamo.AWS.config.update({region: "ap-south-1"});
//var dynamodb = new AWS.DynamoDB();

Patient = dynamo.define('Patient', {
  hashKey: 'patientId',
  rangeKey: 'userId',
  timestamps: true,
  tableName: 'HackathonPatients2',
  schema: {
    //email   : Joi.string().email(),
    firstName: Joi.string(),
    lastName: Joi.string().allow(null),
    patientId: dynamo.types.uuid(),
    userId: Joi.string(),
    age: Joi.number().allow(null),
    gender: Joi.string().allow(null),
    phone: Joi.string().allow(null), //+919876543210   -> 10 digit
  },
   indexes : [{
     hashKey : 'phone',
     rangeKey : 'userId',
     name : 'phone-userId-index',
     type : 'global' 
   }]
});
Patient.prototype.createNew = function (params) {
  Patient.create({
    firstName: params.firstName,
    lastName: params.lastName,
    age: params.age ? params.age : 0,
    gender: params.gender ? params.gender : "NA",
    phone: params.phone ? params.phone : "NA"
  }, function (err, res) {

  })
}
Patient.prototype.findPatientByName = function (params) {
  
  
}

module.exports = Patient;