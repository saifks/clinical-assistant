/**
 * This sample demonstrates a simple skill built with the Amazon Alexa Skills Kit.
 * The Intent Schema, Custom Slots, and Sample Utterances for this skill, as well as
 * testing instructions are located at http://amzn.to/1LzFrj6
 *
 * For additional samples, visit the Alexa Skills Kit Getting Started guide at
 * http://amzn.to/1LGWsLG
 */
const AWS = require("aws-sdk");
const Alexa = require('alexa-sdk');
const Handlers = require('./handlers');
const startModeHandler = Handlers.startModeHandler;
const prescriptionHandler = Handlers.prescriptionHandler;
const newSessionHandler = Handlers.newSessionHandler;
//const registerPatientHandler = Handlers.registerPatientHandler;
//const bookAppointmentHandler = Handlers.bookAppointmentHandler;
if (!AWS.config.region) {
    AWS.config.update({
      region: 'ap-south-1'
    });
  }


let START_NODE = 1;

// --------------- Handlers -----------------------

// Called when the session starts.
exports.handler = function (event, context, callback) {
   // console.log(event)
    const alexa = Alexa.handler(event, context);
    alexa.dynamoDBTableName = 'HackathonAttributes';
    alexa.registerHandlers(newSessionHandler,startModeHandler);
    alexa.execute();
};



